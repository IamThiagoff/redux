<h1 align="center">
  Catálogo Redux
</h1>

<br />

<h1 align="center">
  <img src="https://ik.imagekit.io/lzkiso6iri/READMEs/catalog-redux_EKYWeqZ_NQD.gif?updatedAt=1635427320700">
</h1>

## Indice
- [Sobre](#-sobre)
- [Tecnologias](#-tecnologias)
- [Como baixar e executar o projeto](#-como-baixar-e-executar-o-projeto)

---

## 🔖 Sobre

O **Catálogo Redux** foi desenvolvido no módulo bônus de Redux no bootcamp goStack da Rocketseat com o objetivo de praticar e aprender mais sobre Redux e Redux saga

---

## Tecnologias

O projeto foi desenvolvido utilizando as seguintes tecnologias:

- [ReactJS](https://reactjs.org/)
- [Typescript](https://www.typescriptlang.org/)
- [Redux](https://redux.js.org/)
- [Redux Saga](https://redux-saga.js.org/)
- [Immer](https://immerjs.github.io/immer/)
- [Chakra](https://chakra-ui.com/)

---

## 🗂 Como baixar e executar o projeto


```bash

  # Clonar o repositório
  $ https://github.com//redux.git

  # Entrar no diretório
  $ redux

  # Instalar as dependências
  $ yarn install

  # Iniciar o projeto
  $ yarn start

```
<br />

Desenvolvido por [Thiago]()
